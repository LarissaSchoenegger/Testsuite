<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
    xmlns="http://www.cfs++.org/simulation">
    
    <documentation>
        <title>Testcase for electrostatic forces</title>
        <authors>
            <author>Georg Jank</author>
        </authors>
        <date>2020-11-02</date>
        <keywords>
            <keyword>electrostatic</keyword>
        </keywords>
        <references></references>
        <isVerified>yes</isVerified>
        <description>
            This test case consists of two conducting spheres. Between
            the spheres there is an air gap. The lower sphere is fixed
            and the upper sphere is attached to a spring and damper 
            (i.e. material with poisson number 0 and rayleigh damping).
            A harmonic voltage is applied across the spheres and the 
            electrostatic forces moves the upper sphere towards the 
            lower sphere. The aim of this Testcases is to test the 
            calculation of electrostatic forces in a complex geometry
            (with non-planar surfaces and inconsistent surface force 
            densities).
        </description>
    </documentation>
    
    <fileFormats>
        <input>
            <cdb fileName="ElecForcesConductingSpheres.cdb"/>
        </input>
        <output>
            <hdf5/>
            <!--<text/>-->
        </output>
        <materialData file="mat.xml"/>
    </fileFormats>
    
    <domain geometryType="axi">
        <regionList>
            <region name="S_air" material="air"></region>
            <region name="S_upper_sphere" material="solid"></region>
            <region name="S_lower_sphere" material="solid"></region>
            <region name="S_spring" material="spring"></region>
        </regionList>
        <surfRegionList>
            <surfRegion name="L_upper_sphere"/>
            <surfRegion name="L_lower_sphere"/>
        </surfRegionList>
    </domain>
    
    <fePolynomialList>
        <Lagrange id="Lagrange1">
            <isoOrder>1</isoOrder>
        </Lagrange>
        <Lagrange id="Lagrange2">
            <isoOrder>2</isoOrder>
        </Lagrange>
    </fePolynomialList>
    
    <sequenceStep index="1">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <electrostatic>
                <regionList>
                    <region name="S_air" polyId="Lagrange2"/>
                    <region name="S_spring" polyId="Lagrange2"/>
                </regionList>
                <bcsAndLoads>
                    <potential name="L_upper_sphere" value="1"></potential>
                    <ground name="L_lower_sphere"></ground>
                </bcsAndLoads>
                <storeResults>
                    <!--<elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>-->
                    <surfElemResult type="elecForceDensity">
                        <surfRegionList>
                            <surfRegion name="L_upper_sphere"/>
                        </surfRegionList>
                    </surfElemResult>
                    <!--<surfElemResult type="elecChargeDensity">
                        <surfRegionList>
                            <surfRegion name="L_upper_sphere"/>
                        </surfRegionList>
                    </surfElemResult>    
                    <elemResult type="elecEnergyDensity">
                        <allRegions/>
                    </elemResult>-->
                </storeResults>
            </electrostatic>
        </pdeList>
    </sequenceStep>
    
    <sequenceStep index="2">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <mechanic subType="axi">
                <regionList>
                    <region name="S_upper_sphere" polyId="Lagrange2"/>
                    <region name="S_spring" polyId="Lagrange2"/>
                </regionList>
                <bcsAndLoads>
                    <traction name="L_upper_sphere"> 
                        <sequenceStep index="1">
                            <quantity name="elecForceDensity" pdeName="electrostatic"/>
                            <timeFreqMapping>
                                <constant step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </traction>
                    <fix name="L_fix">
                        <comp dof="r"/>
                        <comp dof="z"/>
                    </fix>
                    <fix name="S_spring">
                        <comp dof="r"/>
                        
                    </fix>
                </bcsAndLoads> 
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                        <!--<nodeList>
                            <nodes name="sensor"/>
                        </nodeList>-->
                    </nodeResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>
    
</cfsSimulation>
