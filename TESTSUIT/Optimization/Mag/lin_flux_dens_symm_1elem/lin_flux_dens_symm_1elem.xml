<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://www.cfs++.org/simulation">
    <documentation>
        <title>Linear normalized squared orientaional magnetic flux density maximization, using symmetry</title>
        <authors>
            <author>Fabian Wein/ Philipp Seebacher</author>
        </authors>
        <date>2018-07-13</date>
        <keywords>
            <keyword>SIMP</keyword>
        </keywords>
        <references>to be in Philipp's thesis :)</references>
        <isVerified>no</isVerified>
        <description>maximization where here the to be maximized domain is also the design domain (it would be normally joch, but this is too slow).
            The current implementation has gradient error of about 3%!! This test is to be updated once fixed.</description>
    </documentation> 
    <fileFormats>
        <input>
            <gmsh fileName="grid2Dsym-rough.msh"/>
        </input>        
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>
    <domain geometryType="plane">
        <variableList>
            <var name="i" value="58.0"/>
            <var name="N" value="100"/>
        </variableList>
        <regionList>
            <region name="spule1_in" material="AIR"/>
            <region name="spule2_out" material="AIR"></region>
            <region name="joch" material="iron"></region>
            <region name="anker" material="iron"></region>
            <region name="OptimierungY" material="iron"></region>
            <region name="air" material="AIR"></region>
        </regionList>
    </domain>    
    <sequenceStep index="1">
        <analysis>
            <static>
            </static>
        </analysis>
        <pdeList>
            <magnetic>
                <regionList>
                    <region name="spule1_in"/>
                    <region name="spule2_out"/>
                    <region name="joch" />
                    <region name="anker"/>
                    <region name="air"/>
                    <region name="OptimierungY"/>
                </regionList>
                <bcsAndLoads>
                    <fluxParallel name="FluxParallel_V">
                        <comp dof="y"/>
                    </fluxParallel>
                    <fluxParallel name="FluxParallel_h">
                        <comp dof="x"/>
                    </fluxParallel>
                </bcsAndLoads>
                <coilList>
                    <coil id="Spule1">
                        <source type="current" value="i*N"/>
                        <part id="1">
                            <regionList>
                                <region name="spule1_in"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="z" value="1"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="500e-6"/>
                        </part>
                        <part id="2">
                            <regionList>
                                <region name="spule2_out"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="z" value="1"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="500e-6"/>
                        </part>
                    </coil>
                </coilList>
                <storeResults>
                    <nodeResult type="magPotential">
                        <allRegions/>
                    </nodeResult>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="pseudoDensity">
                      <allRegions/>
                    </elemResult>
                    <elemResult type="physicalPseudoDensity">
                       <allRegions/>
                    </elemResult>
                    <elemResult type="optResult_1">
                        <regionList>
                          <region name="OptimierungY"/>
                        </regionList>
                    </elemResult>                    
                    <surfElemResult type="magForceMaxwellDensity" >
                        <surfRegionList>
                            <surfRegion name="Force" outputIds="hdf5" neighborRegion="air" writeAsHistResult="no" />
                        </surfRegionList>
                    </surfElemResult>
                    <surfRegionResult type="magForceMaxwell">
                        <surfRegionList>
                            <surfRegion name="Force" outputIds="txt" neighborRegion="air"/>
                        </surfRegionList>                            
                    </surfRegionResult>
                    <surfRegionResult type="magForceVWP">
                        <surfRegionList>
                            <surfRegion name="Force" outputIds="txt" neighborRegion="air"/>
                        </surfRegionList>
                    </surfRegionResult>
                </storeResults>
            </magnetic>
        </pdeList>
        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                         <!--exportLinSys /--> 
                        <matrix reordering="noReordering"/>
                    </standard>
                </solutionStrategy>
                <solverList>
                    <pardiso></pardiso>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
    
    <optimization>
        <costFunction type="sqrMagFluxDensY" task="maximize" region="OptimierungY">
            <stopping queue="100" value="1.0E-4" type="designChange" />
        </costFunction>
        
        <constraint type="volume" bound="lowerBound" value="0.45" mode="constraint" />
        <constraint type="greyness" access="physical" mode="observation"/>
        
        <optimizer type="snopt" maxIterations="100" >
            <snopt>
                <option name="major_optimality_tolerance" type="real" value="1e-8" />
                <option name="verify_level" type="integer" value="3"/>
            </snopt>
        </optimizer>
        
        <ersatzMaterial material="magnetic" method="simp">
            <regions>
                <region name="OptimierungY"/>
            </regions>
            <!--  currently necessary to switch off due to a bug in lec -->
            <designSpace local_element_cache="false"/>
            <design name="density" initial="0.5" upper="1.0" lower=".1" />
            <transferFunction type="simp" application="magnetic" param="1"/>
            <result value="costGradient" id="optResult_1"/>
            <!--             <result value="costGradient" id="optResult_2" detail="finiteDiffCostGradRelError"/> -->
            <!--             <result value="costGradient" id="optResult_3" detail="finiteDiffCostGrad"/> -->
            <export write="iteration" save="all"/> 
        </ersatzMaterial>
        <!-- write also adjoint solution at .5 timesteps, take care when reading pyhsical data! -->
        <commit mode="both_cases" stride="1"/>
    </optimization>  
    
    
</cfsSimulation>
