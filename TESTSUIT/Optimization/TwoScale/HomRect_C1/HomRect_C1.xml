<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>homrect_c1 min complinace</title>
    <authors>
      <author>Thomas Guess</author>
    </authors>
    <date>2014-01-14</date>
    <keywords>
      <keyword>optimization</keyword>
      <keyword>filter</keyword>
    </keywords>
    <references>Bensoe, Kikuchi; Generating optimal topologies in
      structural design using a homogenization method</references>
    <isVerified>yes</isVerified>
    <description>Standard example for material optimization using
      homogenized tensors from cross shaped base cells</description>
  </documentation>

  <fileFormats>
    <output>
      <hdf5 />
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="mech" material="99lines" />
    </regionList>
  </domain>

  <sequenceStep>
    <analysis>
      <static />
    </analysis>

    <pdeList>
      <mechanic subType="planeStress">
        <regionList>
          <region name="mech" />
        </regionList>

        <bcsAndLoads>
          <fix name="support">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <force name="load">
            <comp dof="y" value="-1"/>
          </force>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions />
          </nodeResult>
          <elemResult type="mechTensor">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_1">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_3">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_4">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_5">
            <allRegions />
          </elemResult>
          <elemResult type="optResult_6">
            <allRegions />
          </elemResult>

        </storeResults>
      </mechanic>

    </pdeList>

  </sequenceStep>

  <optimization>
    <costFunction type="compliance" multiple_excitation="false" design="allDesigns">
      <local normalize="true" power="1" />
      <stopping queue="999" value="1.0E-5" type="designChange" />
    </costFunction>

    <constraint type="globalTwoScaleVolume" design="allDesigns" bound="upperBound" value="0.5" linear="false" access="filtered">
      <local normalize="true" power="1" />
    </constraint>

    <optimizer type="snopt" maxIterations="4">
      <snopt>
        <option type="real" name="major_optimality_tolerance"
          value="1e-6" />
        <option type="integer" name="verify_level" value="0" />
      </snopt>
    </optimizer>
    <ersatzMaterial method="paramMat" material="mechanic" region="mech">
     <filters>
       <filter type="density" design="stiff1" neighborhood="maxEdge" value="1.5"/>
       <filter type="density" design="stiff2" neighborhood="maxEdge" value="1.5"/>
      </filters>
      <paramMat>
        <designMaterials>
          <!-- format of tensordata: a b 11 12 22 33 where a and b are percent -->
          <designMaterial type="hom-rect-C1">
            <param name="rotAngle" value="0." />
            <param name="shear1" value="0.5" />
            <!-- load material catalogue from offline phase -->
            <homRectC1 file="coeff_hom_frame_2000_filt_void1e-9.xml" interpolation="c1"/>
          </designMaterial>
        </designMaterials>
      </paramMat>
      <design name="stiff1" region="mech" initial="0.2" lower="0.001"
        upper="1." />
      <design name="stiff2" region="mech" initial="0.2" lower="0.001"
        upper="1." />
      <result value="design" design="stiff1" id="optResult_1"
        access="smart" />
      <result value="design" design="stiff2" id="optResult_2"
        access="smart" />
      <result value="design" design="stiff1" id="optResult_4"
        access="plain" />
      <result value="design" design="stiff2" id="optResult_5"
        access="plain" />
      <export />
    </ersatzMaterial>
    <commit stride="500" />
  </optimization>
</cfsSimulation>
