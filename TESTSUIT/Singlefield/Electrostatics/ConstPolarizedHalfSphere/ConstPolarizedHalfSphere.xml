<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  
  <documentation>
    <title>PolarizedHalfSphere3D</title>
    <authors>
      <author>F. Holzberger</author>
      <author>F. Heidegger</author>
      <author>J. Ellmenreich</author>
    </authors>
    <date>2021-01-19</date>
    <keywords>
      <keyword>electrostatic</keyword>
    </keywords>
    <references> http://web.mit.edu/6.013_book/www/chapter6/6.3.html </references>
    <isVerified>yes</isVerified>
    <description> 
      We simulate a halfsphere with radius R=0.1 and constant polarization Pol=1e-7 and compare it
      to the analytic solution. To do this we need a infinite mapping, since the potential
      tends towards zero far away from the sphere. The analytic solution corresponds to:
        phi_{sphere} = Pol/(3*eps_0)*r*cos(theta)
        phi_{air} = Pol*R^3/(3*eps_0)*cos(theta)/r^2
      Where r is the spherical coordinate and theta the angle between z and r (cos(theta) = z/r)
    </description>
  </documentation>

  <fileFormats>
    <input>
      <cdb fileName="ConstPolarizedHalfSphere.cdb"/>
    </input>
    <output>
      <hdf5 />
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="3d">
    <variableList>
      <!-- The variables for later use were defined here -->
      <var name="Pol" value="1.0E-7"/>
      <var name="eps_0" value="8.854187E-12"/>
    </variableList>
    <regionList>
      <region name="extern" material="Vacuum" />
      <region name="intern" material="Vacuum" />
		</regionList>
  </domain>


  <sequenceStep>
    <analysis>
      <static />
    </analysis>

    <pdeList>

      <!-- To set the exact solution as Dirichlet uncomment the dampingList and switch from ground to potential -->
      <electrostatic>
        <regionList>
          <region name="extern" dampingId="Mapping" />
          <region name="intern" />
        </regionList>
        <dampingList>
          <mapping id="Mapping">
           <propRegion>
              <direction comp="x" min="0.0" max="0.3"/>
              <direction comp="y" min="-0.3" max="0.3"/>
              <direction comp="z" min="-0.3" max="0.3"/>
            </propRegion>
              <type> tangens</type>
              <dampFactor> 0.25 </dampFactor>
          </mapping>
        </dampingList>
        <bcsAndLoads>
          <ground name="boundary_surrounding"/>
          <!--<potential name="boundary_surrounding" value="Pol*0.1*0.1*0.1/(3*eps_0)*z/sqrt(x*x+y*y+z*z)/(x*x+y*y+z*z) "/>			-->
					<polarization name="intern">
						<comp dof="x" value="0.0"/>
						<comp dof="y" value="0.0"/>
						<comp dof="z" value="Pol"/>
					</polarization>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="elecPotential">
            <allRegions />  
          </nodeResult>
          <elemResult type="elecFieldIntensity">
            <allRegions />
          </elemResult>
        </storeResults>
      </electrostatic>


    </pdeList>

  </sequenceStep>

</cfsSimulation>
