from math import pi;
# for meshing
r_core_outer = 40.0e-3;
r_core_inner = 20.0e-3;
r_wire = 1.5e-3;
core_thickness = 20.0e-3;
r_domain=2*r_core_outer;
air_dist = r_domain-r_core_outer;

